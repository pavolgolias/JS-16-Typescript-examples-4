"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PIval = 3.14;
function calculateCircumstances(diameter) {
    return diameter * exports.PIval;
}
exports.calculateCircumstances = calculateCircumstances;
