export const PIval = 3.14;

export function calculateCircumstances(diameter: number) : number {
    return diameter * PIval;
}
